import axios from 'axios'

const Api = {}

const types = ['text', 'text_btn', 'table', 'list']

let typeValidate = (type) => {
  return types.indexOf(type) !== -1
}

Api.install = (Vue, options) => {
  Vue.prototype.$api = {
    getOptions () {
      return axios.get('/api/autocomplete')
    },

    getItem (itemType) {
      console.log(itemType)
      if (typeValidate(itemType)) {
        return axios.post('/api/command', {message: itemType})
      } else {
        console.error('error loading item', itemType)
      }
    }
  }
}

export default Api
